from os import path

print("Bonjour tout le monde")


def choose_file_name(msg):
    file_name = input(msg) + ".txt"
    # Creating file if it doesn't exist
    if not path.exists(file_name):
        open(file_name, 'x')
    return file_name


def add_text_to_file():
    file_name = choose_file_name('\n\n\n' + "Merci d'entrer le nom de votre fichier une nouvelle fois : ")
    if file_name == '':
        print('Erreur, merci de rentrer un nom de fichier non vide !')
    else:
        text = input("Merci d'ajouter du texte :")
        file = open(file_name, "a")
        file.write(text)
        file.close()


def display_file_content():
    file = choose_file_name("Merci d'entrer le nom d'un fichier EXISTANT : ")
    with open(file, "r") as fic:
        print(fic.read())


def empty_file():
    file = choose_file_name("Merci d'entrer le nom du fichier à vider : ")
    with open(file, "w") as fic:
        fic.close()


def generate_menu():
    action = input("Merci de choisir une action :" + '\n'
                     "1: Choisir un fichier" + '\n'
                       "2: Ajouter du contenu dans ce fichier : " + '\n'
                            "3: Afficher le contenu du fichier : " + '\n'
                                "4: Vider un fichier : " + '\n'
                                    "5: Arrêter le programme: " + '\n')
    if action == "1":
        choose_file_name("Merci d'entrer le nom d'un fichier : ")
        generate_menu()
    elif action == "2":
        add_text_to_file()
        generate_menu()
    elif action == "3":
        display_file_content()
        generate_menu()
    elif action == "4":
        empty_file()
        generate_menu()
    elif action == "5":
        exit()


generate_menu()
