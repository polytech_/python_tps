import csv
from datetime import date


class Date:
    def __init__(self, param_day, param_month, param_year):
        if 31 >= param_day >= 1 and 1 <= param_month <= 12 and param_year > 0:
            self.day = param_day
            self.month = param_month
            self.year = param_year
        else:
            print("Merci d'entrer une valeur de mois entre 1 et 12 et/ou une valeur"
                  "de jour entre 1 et 31 et/ou une valeur d'année positive")

    def __eq__(self, param_date):
        return self.day == param_date.day and self.month == param_date.month and self.year == param_date.year

    def __lt__(self, param_date):
        inf = False
        # day difference
        if self.year < param_date.year:
            inf = True
        elif self.year == param_date.year:
            if self.month < param_date.month:
                inf = True
            else:
                if self.day < param_date.day:
                    inf = True
        return inf

    def __str__(self):
        return str(self.day) + "/" + str(self.month) + "/" + str(self.year)

class Etudiant:

    #Accesseurs ?

    def __init__(self, fname_param, lname_param, age_param):
        self.lname = lname_param
        self.fname = fname_param
        self.age = age_param

    def adresselec(self):
        return self.fname.lower() + "." + self.lname.lower() + "@etu.univ-tours.fr"


    def __str__(self):
        return "Nom : " + str(self.lname)  + " Prénom : " + str(self.fname) + " Age : " + str(self.age)


# date1 = Date(28, 1, 1997)
# date2 = Date(29, 1, 1997)
# print(date1 < date2)

# etd = Etudiant("Othmane", "Allamou", 23)
# print(etd.adresselec())
# print(etd.age)



#Lecture du fichier CSV
def lecture_fichier_CSV():
    list_etd = []
    current_year = date.today().year

    with open('fichetu.csv', 'r') as fichetu:
        csv_reader = csv.reader(fichetu, delimiter=';')
        for line in csv_reader:
            born_year = line[2].split('/')[2]
            _etd = Etudiant(line[0], line[1], current_year - int(born_year))
            list_etd.append(_etd.__str__())
        return list_etd


#listetd = lecture_fichier_CSV()

print(lecture_fichier_CSV())