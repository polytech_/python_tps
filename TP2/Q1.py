from tkinter import *

input_value = ""


def entree(numero):
    global input_value
    input_value = input_value + str(numero)

    equation.set(input_value)

def boutonegal():
    try:
        global input_value
        total = str(eval(input_value))
        equation.set(total)

    except:
        equation.set("Erreur, Appuyez sur C et reessayez")
        input_value = ""


def clear():
    global input_value
    input_value = input_value[:-1]
    equation.set(input_value)

def allclear():
    global input_value
    input_value = ""
    equation.set("")


if __name__=="__main__":

    fenetre = Tk()
    menubar = Menu(fenetre)
    fenetre.config(menu=menubar)
    menuaide = Menu(menubar,tearoff=0)
    menubar.add_cascade(label="aide",menu=menuaide)
    fenetre.configure(background="grey")
    fenetre.title("Casio mdr7a")
    fenetre.geometry("600x300")
    equation = StringVar()
    expression_field = Entry(fenetre, textvariable=equation)
    expression_field.grid(columnspan=10, ipadx=300)

    button1 = Button(fenetre, text=' 1 ',fg='#008000', bg='black', command=lambda: entree(1), height = 1 , width = 7 )
    button1.grid(row=2,column=0)

    button2 = Button(fenetre, text=' 2 ',fg='#008000', bg='black', command=lambda: entree(2), height = 1 , width = 7 )
    button2.grid(row=2,column=1)

    button3 = Button(fenetre, text=' 3 ',fg='#008000', bg='black', command=lambda: entree(3), height = 1 , width = 7 )
    button3.grid(row=2,column=2)

    button4 = Button(fenetre, text=' 4 ',fg='#008000', bg='black', command=lambda: entree(4), height = 1 , width = 7 )
    button4.grid(row=3,column=0)

    button5 = Button(fenetre, text=' 5 ',fg='#008000', bg='black', command=lambda: entree(5), height = 1 , width = 7 )
    button5.grid(row=3,column=1)

    button6 = Button(fenetre, text=' 6 ',fg='#008000', bg='black', command=lambda: entree(6), height = 1 , width = 7 )
    button6.grid(row=3,column=2)

    button7 = Button(fenetre, text=' 7 ',fg='#008000', bg='black', command=lambda: entree(7), height = 1 , width = 7 )
    button7.grid(row=4,column=0)

    button8 = Button(fenetre, text=' 8 ',fg='#008000', bg='black', command=lambda: entree(8), height = 1 , width = 7 )
    button8.grid(row=4,column=1)

    button9 = Button(fenetre, text=' 9 ',fg='#008000', bg='black', command=lambda: entree(9), height = 1 , width = 7 )
    button9.grid(row=4,column=2)

    button0 = Button(fenetre, text=' 0 ',fg='#008000', bg='black', command=lambda: entree(0), height = 1 , width = 7 )
    button0.grid(row=5,column=0)

    buttonplus = Button(fenetre, text=' + ',fg='#008000', bg='black', command=lambda: entree("+"), height = 1 , width = 7 )
    buttonplus.grid(row=2,column=5)

    buttonmoins = Button(fenetre, text=' - ',fg='#008000', bg='black', command=lambda: entree("-"), height = 1 , width = 7 )
    buttonmoins.grid(row=3,column=5)

    buttonmult = Button(fenetre, text=' * ',fg='#008000', bg='black', command=lambda: entree("*"), height = 1 , width = 7 )
    buttonmult.grid(row=2,column=4)

    buttondiv = Button(fenetre, text=' / ',fg='#008000', bg='black', command=lambda: entree("/"), height = 1 , width = 7 )
    buttondiv.grid(row=3,column=4)

    buttonplus = Button(fenetre, text=' + ',fg='#008000', bg='black', command=lambda: entree("+"), height = 1 , width = 7 )
    buttonplus.grid(row=2,column=5)

    buttonegal = Button(fenetre, text=' = ',fg='#008000', bg='black', command=lambda: boutonegal(), height = 1 , width = 7 )
    buttonegal.grid(row=4,column=5)

    buttonclear = Button(fenetre, text=' C ',fg='#008000', bg='black', command=lambda: clear(), height = 1 , width = 7 )
    buttonclear.grid(row=4,column=4)

    buttonallclear = Button(fenetre, text=' AC ',fg='#008000', bg='black', command=lambda: allclear(), height = 1 , width = 7 )
    buttonallclear.grid(row=5,column=4)


    fenetre.mainloop()

